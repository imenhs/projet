package com.ecommerce.microcommerce.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecommerce.microcommerce.dao.ProductDao;
import com.ecommerce.microcommerce.model.Product;

@RestController

@RequestMapping("/api")
public class ProductController {

	@Autowired
	private ProductDao productDao;

	@GetMapping(value = "/Produits")
	public List<Product> listeProduits() {
		return productDao.findAll();
	}

	// Prpduit/{id} : chercher un produit par son id

	@GetMapping(value = "/Produits-GET/{id}")

	public Product afficherUnProduit(@PathVariable int id) {

		return productDao.findById(id);
	}

	@PostMapping(value = "/Ajout-Produits")
	public void ajouterProduit(@RequestBody Product product) {

		productDao.save(product);
	}

	// Récupérer la liste des produits
	/*
	 * @RequestMapping(value = "/Produits", method = RequestMethod.GET) public
	 * String listeProduits() { return "Un exemple de produit"; }
	 */
	@GetMapping(value = "test/produits/{prixLimit}")
	public List<Product> testeDeRequetes(@PathVariable int prixLimit) {
		return productDao.findByPrixGreaterThan(prixLimit);

	}

	@DeleteMapping(value = "/Produits-DEL/{id}")
	public void supprimerProduit(@PathVariable int id) {

		productDao.deleteById(id);
	}

	@PutMapping(value = "/Put-Produits")
	public void updateProduit(@RequestBody Product product) {

		productDao.save(product);
	}

}
